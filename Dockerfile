FROM nexus.sulamerica.br:8000/corporativo/alpine-springboot-sas:latest

MAINTAINER Jazon Santos <jazon.santos@t-systems.com.br>

ADD module1/target/job-base-rederef.jar ${HOME}/app.jar
ADD info.log ${HOME}/