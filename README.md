#Buildar na maquina a versão do cofre que não está disponivel na master
https://bitbucket.org/sulamericaic/cofre-senha/src/master/

#Instalar o plugin do Lombok no Intellij

#Adicionar no hosts
10.225.213.123  acessourgente.sulamerica.br

#Run inside /lib/com/oracle/ojdbc/6 para instalar a dependencia do Oracle Jdbc
mvn install:install-file -Dfile=ojdbc-6.jar -DgroupId=com.oracle -DartifactId=ojdbc -Dversion=6 -Dpackaging=jar

Para rodar o projeto por linha de comando, rodar o comando a baixo na raiz do projeto job-base-rederef

./mvnw install && ./mvnw spring-boot:run -pl module1


Para rodar pelo intellij basta ir até a classe job-base-rederef/module1/src/main/java/hello/app/Application.java
E rodar o main. Depois é só rodar pelo Run > Run 'Aplication'


Para rodar através do jar.
rodar o ./mvnw install vai criar uma pasta chamada Target dentro terá um .jar.
ou
java -jar module1/target/job-base-rederef.jar