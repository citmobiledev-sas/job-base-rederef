package hello.app;

import com.sulamerica.base.MyService;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class ApplicationTest {

//    @Autowired
//    private MyService myService;

    @Test
    public void contextLoads() {
        assertThat(new MyService().message()).isNotNull();
    }

}
