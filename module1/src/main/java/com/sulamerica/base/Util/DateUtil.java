package com.sulamerica.base.Util;

import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;

@Component
public class DateUtil {

    public LocalDateTime getDateTimeNow() {
        LocalDateTime localDateTime = LocalDateTime.now(ZoneId.of("America/Sao_Paulo"));

        return localDateTime;
    }
}
