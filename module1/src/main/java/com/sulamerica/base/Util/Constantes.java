/**
 * 
 */
package com.sulamerica.base.Util;

/**
 * Classe para constantes do projeto.
 * 
 * @author thomasp
 * 
 */
public final class Constantes {

	private Constantes() {
	}

	/**
	 * Constantes para status do retorno do google Geocoding.
	 */
	public static final String STATUS_OK = "OK";

	/**
	 * Constantes para status do retorno do google Geocoding.
	 */
	public static final String STATUS_ZERO_RESULTS = "ZERO_RESULTS";

	/**
	 * Constantes para status do retorno do google Geocoding.
	 */
	public static final String STATUS_OVER_QUERY_LIMIT = "OVER_QUERY_LIMIT";

	/**
	 * Constantes utilizada para o Log Estatistico
	 */
	public static final String SEPARADOR_TOSTRING_GENERICO = ";";
	public static final Long TIPO_TRANS_REDE_REF = 4L;
	public static final String IDC_STATUS_TRANS_OK = "O";
	public static final String IDC_STATUS_TRANS_EXEC = "E";
	public static final String IDC_STATUS_TRANS_FALHA = "F";
	public static final String IDC_CANAL_TRANS = "GR";
	public static final String SEPARADOR_CAMPOS_COMANDO = ";;";
	public static final String SEPARADOR_COMANDOS = "##";

	public static final int HASH_MAGIC_PRIME_31 = 31;
	public static final int HASH_MAGIC_NUMBER_32 = 32;
	public static final int HASH_MAGIC_NUMBER_1 = 1;
	public static final int HASH_MAGIC_NUMBER_0 = 0;

	public static final String SEPARADOR_VIRGULA = ",";

	/** Constantes de hash para passar no PMD. */
	public static final String SUPPRESS_WARNINGS_UNCHECKED = "unchecked";
	public static final String SUPPRESS_WARNINGS_RAWTYPES = "rawtypes";
}
