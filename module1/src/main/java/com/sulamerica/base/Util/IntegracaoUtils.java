package com.sulamerica.base.Util;

import java.lang.reflect.Method;

/**
 * @author lponce
 * 
 */
public class IntegracaoUtils {

	public static final int FORMAT_METHOD_TRES = 3;
	
    /**
     * @param obj
     * @return
     */
    public String genericToString(Object obj) {

        Class<? extends Object> c = obj.getClass();

        String methods = "";

        if (obj.getClass().toString().startsWith("class java.lang")) {
            methods += String.format("%s : %s%s", obj.getClass().toString()
                    .replace("class java.lang.", ""), obj,
                    Constantes.SEPARADOR_TOSTRING_GENERICO);
        } else {

            Method m[] = c.getDeclaredMethods();

            for (Method method : m) {

                if (method.getName().startsWith("get")
                        || method.getName().startsWith("is")) {
                    try {
                        methods += String.format("%s : %s%s",
                                formatMethodName(method), method.invoke(obj),
                                Constantes.SEPARADOR_TOSTRING_GENERICO);
                    } catch (Exception e) {
                        methods += "Não foi possivel recuperar dados do objeto";
                    }

                }

            }

        }

        return methods;
    }

    private String formatMethodName(Method method) {
        String retorno;

        if (method.getName().startsWith("is")) {
            retorno = method.getName().substring(2);
        } else {
            retorno = method.getName().substring(FORMAT_METHOD_TRES);
        }
        return retorno;
    }
}
