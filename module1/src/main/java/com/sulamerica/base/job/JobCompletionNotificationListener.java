package com.sulamerica.base.job;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sulamerica.base.MyService;
import com.sulamerica.base.model.RedeReferenciadaBairro;
import com.sulamerica.base.service.BairroService;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class JobCompletionNotificationListener implements ApplicationRunner {
    private static final Logger LOG =
            LoggerFactory.getLogger(JobCompletionNotificationListener.class);

    public static int counter;

    @Autowired
    private MyService myService;

    @Autowired
    private BairroService bairroService;

    @Override
    public void run(ApplicationArguments args) throws Exception {

        LOG.info("Parametros e valor");
        for(String valor: args.getOptionNames()) {
            LOG.info(valor+": {}", args.getOptionValues(valor));
        }
        counter++;

        LOG.info(myService.message() + " chamou modulo 2");

        List<RedeReferenciadaBairro> bairros = bairroService.listarBairrosNaoGeocodificados();

        ObjectMapper objectMapper = new ObjectMapper();
        LOG.info(objectMapper.writeValueAsString(bairros) + " chamou bairro");



        LOG.info("---------------------------------");
        RedeReferenciadaBairro b = bairroService.getById(110115L);
        LOG.info(objectMapper.writeValueAsString(b));

    }
}