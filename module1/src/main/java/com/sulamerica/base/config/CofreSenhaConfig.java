package com.sulamerica.base.config;

import br.com.sulamerica.cofresenha.dto.Cofre;
import br.com.sulamerica.cofresenha.exception.CofresenhaException;
import br.com.sulamerica.cofresenha.service.CofresenhaService;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static java.util.Objects.isNull;

@Configuration
@Slf4j
@Data
public class CofreSenhaConfig {

    @Value("${br.sulamerica.cofresenha.oauth-request-cofre.id}")
    private Integer identificador;

    @Value("${br.sulamerica.cofresenha.oauth-request-cofre.url}")
    private String url;

    @Bean
    public Cofre cofre(CofresenhaService cofresenhaService) {
        log.info("create datasource, idCofre: {}", identificador);
        log.info("create url, url: {}", url);

        Cofre cofre = null;

        try {
            cofre = cofresenhaService.buscarSegredoCofre(identificador);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        if (isNull(cofre) || HttpStatus.SC_OK != cofre.getResponse().getStatus()) {
            throw new CofresenhaException("Não foi possível acessar senha no cofre!!");
        }

        return cofre;
    }
}
