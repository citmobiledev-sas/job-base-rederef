/**
 *
 */
package com.sulamerica.base.service;

import com.sulamerica.base.Util.Constantes;
import com.sulamerica.base.Util.IntegracaoUtils;
import org.aspectj.lang.JoinPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lponce
 *
 */
@Service(value = "logServicesImplRedeRef")
public class LogServicesImpl implements LogServices {

	/**
	 * LOGGER.
	 */
    private Logger logger = LoggerFactory.getLogger(this.getClass());

	public static final Double TRANS_MILISEGUNDOS = 1000.0;

//	@Autowired
//	private RedeReferenciadaLogTransacaoDao redeRefLogTransacaoDao;
//
//	@Autowired
//	private RedeReferenciadaLogErroDao redeRefLogErroDao;

	private IntegracaoUtils integracaoUtils = new IntegracaoUtils();

	private Map<JoinPoint, Long> codTransacao = new HashMap<JoinPoint, Long>();
	private Map<JoinPoint, Long> tempoTransacao = new HashMap<JoinPoint, Long>();
	private String nmClasse;
	private String nmMetodo;
	private String ipCliente;
	private String idcCanalTrans;
	private String idcStatusTrans;
	private Date inicioTransacao;

	@Override
	public void logClientIP(JoinPoint jp) {
		Object[] args = jp.getArgs();

		if (args[0] instanceof String) {
			ipCliente = (String) args[0];
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false, isolation = Isolation.READ_COMMITTED)
	public void logInicioGravacao(JoinPoint jp) throws UnknownHostException {

		nmClasse = jp.getTarget().getClass().toString();
		nmMetodo = jp.getSignature().getName();

		insereLog(jp, Constantes.TIPO_TRANS_REDE_REF);

	}

	private void insereLog(final JoinPoint joinPoint, final Long tipoTrans)
			throws UnknownHostException {

		logger.info("Inserindo log - Inicio: " + System.currentTimeMillis());

		idcCanalTrans = Constantes.IDC_CANAL_TRANS;

		inicioTransacao = new Date(System.currentTimeMillis());
		tempoTransacao.put(joinPoint, System.currentTimeMillis());


        //TODO regra para criar o que será salvo e implementadar o código para salvar no Bando de Dados os logs do Aspectj"
        logger.info("//TODO Aqui deve ser implementado o código para salvar no Bando de Dados os logs do Aspectj");

		logger.info("Inserindo log - nome_do_log: ");
		logger.info("Inserindo log - Fim: " + System.currentTimeMillis());
	}

	@Override
//	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false, isolation = Isolation.READ_COMMITTED)
	public void logDAOException(JoinPoint jp, Throwable exception) {

		nmClasse = jp.getTarget().getClass().toString();
		nmMetodo = jp.getSignature().getName();

	}

	@Override
//	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false, isolation = Isolation.READ_COMMITTED)
	public void logException(JoinPoint jp, Throwable exception) {

		List<String> dscErro = new ArrayList<String>();

		// Obtem o ultimo codtransacao da lista
		Long cdTransacao = codTransacao.get(jp);
		Long iniTransacao = tempoTransacao.get(jp);

		logger.info("Exception log - cdTransacao (" + cdTransacao
				+ ") - inicioTransacao (" + inicioTransacao + ")");

		String resumoErro = exception.getClass().toString();
		Date dataHoraErro = new Date(System.currentTimeMillis());

		// Adiciona a mensagem
		dscErro.add(exception.getMessage());

		// Adiciona a causa
		if (exception.getCause() != null) {
			dscErro.add(exception.getCause().getMessage());
		}

		for (StackTraceElement st : exception.getStackTrace()) {
			dscErro.add(st.toString());
		}

		Date fimTransacao = new Date(System.currentTimeMillis());
		Double timeTransacao = ((System.currentTimeMillis() - iniTransacao) * 1.0)
				/ TRANS_MILISEGUNDOS;

		logger.info("Exception log - tempoTransacao: "
				+ timeTransacao.longValue());

		idcStatusTrans = Constantes.IDC_STATUS_TRANS_FALHA;

        //TODO regra para criar o que será salvo e implementadar o código para salvar no Bando de Dados os logs do Aspectj"
        logger.info("//TODO Aqui deve ser implementado o código para salvar no Bando de Dados os logs do Aspectj");

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.sulamerica.susis.redereferenciada.sync.log.LogServices#logFimGravacao
	 * (org.aspectj.lang.JoinPoint)
	 */
	@Override
//	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false, isolation = Isolation.READ_COMMITTED)
	public void logFimGravacao(JoinPoint jp) {

		// Obtem o ultimo codtransacao da lista
		Long cdTransacao = codTransacao.remove(jp);
		Long iniTransacao = tempoTransacao.remove(jp);

		logger.info("Fim log - cdTransacao: " + cdTransacao
				+ " inicioTransacao: " + inicioTransacao);

		Date fimTransacao = new Date(System.currentTimeMillis());
		Double timeTransacao = ((System.currentTimeMillis() - iniTransacao) * 1.0)
				/ TRANS_MILISEGUNDOS;

		logger.info("Fim log - tempoTransacao: " + timeTransacao.longValue());

		idcStatusTrans = Constantes.IDC_STATUS_TRANS_OK;

        //TODO regra para criar o que será salvo e implementadar o código para salvar no Bando de Dados os logs do Aspectj"
        logger.info("//TODO Aqui deve ser implementado o código para salvar no Bando de Dados os logs do Aspectj");

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.sulamerica.susis.redereferenciada.sync.log.LogServices#
	 * logInicioSincronizacao(org.aspectj.lang.JoinPoint)
	 */
	@Override
//	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false, isolation = Isolation.READ_COMMITTED)
	public void logInicioSincronizacao(JoinPoint jp)
			throws UnknownHostException {

		nmClasse = jp.getTarget().getClass().toString();
		nmMetodo = jp.getSignature().getName();
		idcCanalTrans = Constantes.IDC_CANAL_TRANS;

		insereLog(jp, Constantes.TIPO_TRANS_REDE_REF);

	}

}
