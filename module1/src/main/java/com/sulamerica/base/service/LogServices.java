/**
 * 
 */
package com.sulamerica.base.service;

import org.aspectj.lang.JoinPoint;

import java.net.UnknownHostException;

/**
 * @author lponce
 *
 */
public interface LogServices {
	
	void logClientIP(JoinPoint jp);
	
	void logInicioGravacao(JoinPoint jp) throws UnknownHostException;
	
	void logDAOException(JoinPoint jp, Throwable exception);
	
	void logException(JoinPoint jp, Throwable exception);
	
	void logFimGravacao(JoinPoint jp);

	void logInicioSincronizacao(JoinPoint jp) throws UnknownHostException;

}
