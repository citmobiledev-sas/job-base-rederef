package com.sulamerica.base.service;

import com.sulamerica.base.model.RedeReferenciadaBairro;
import com.sulamerica.base.repository.BairroDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BairroService {

    @Autowired
    BairroDAO bairroDAO;


    public List<RedeReferenciadaBairro> listarBairrosNaoGeocodificados() {
        return bairroDAO.listarBairrosNaoGeocodificados();
    }

    public RedeReferenciadaBairro getById(Long id){
        return bairroDAO.selectById(id);
    }

}
