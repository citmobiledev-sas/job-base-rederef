package com.sulamerica.base.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author cmiranda
 */
@Entity(name = "RedeReferenciadaBairro")
@Table(name = "SDE_BAIRRO_GEOREF")
@SequenceGenerator(name = "sqRedeReferenciadaBairro",
					sequenceName = "SDE_SQ_BAIR_GEOR#COD_BAIR_GEOR")
public class RedeReferenciadaBairro implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -9142986621903364472L;

	/**
	 * @uml.property name="id"
	 */
	@Id
	@Column(name = "COD_BAIRRO_GEOREF", nullable = false)
	@GeneratedValue(generator = "sqRedeReferenciadaBairro", strategy = GenerationType.SEQUENCE)
	private Long id;

	/**
	 * @uml.property name="uf"
	 */
	@Column(name = "SIG_UF")
	private String uf;

	/**
	 * @uml.property name="municipio"
	 */
	@Column(name = "NME_MUNICIPIO")
	private String municipio;

	/**
	 * @uml.property name="bairro"
	 */
	@Column(name = "NME_BAIRRO_PADRAO")
	private String bairro;

	/**
	 * @uml.property name="bairro"
	 */
	@Column(name = "NME_SUB_BAIRRO")
	private String subBairro;

	/**
	 * @uml.property name="dataAtualizacao"
	 */
	@Column(name = "DAT_ALTERACAO")
	@Temporal(TemporalType.DATE)
	private Date dataAlteracao = new Date();

	/**
	 * @return id
	 * @uml.property name="id"
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id teste
	 * @uml.property name="id"
	 */
	public void setId(final Long id) {
		this.id = id;
	}

	/**
	 * @return uf
	 * @uml.property name="uf"
	 */
	public String getUf() {
		return uf;
	}

	/**
	 * @param uf - sigla do estado
	 * @uml.property name="uf"
	 */
	public void setUf(final String uf) {
		this.uf = uf;
	}

	/**
	 * @return municipio
	 * @uml.property name="municipio"
	 */
	public String getMunicipio() {
		return municipio;
	}

	/**
	 * @param municipio - nome do municipio
	 * @uml.property name="municipio"
	 */
	public void setMunicipio(final String municipio) {
		this.municipio = municipio;
	}

	/**
	 * @return bairro
	 * @uml.property name="bairro"
	 */
	public String getBairro() {
		return bairro;
	}

	/**
	 * @param bairro - nome do bairro
	 * @uml.property name="bairro"
	 */
	public void setBairro(final String bairro) {
		this.bairro = bairro;
	}

	/**
	 * @return - subBairro
	 * @uml.property name="subBairro"
	 */
	public String getSubBairro() {
		return subBairro;
	}


	/**
	 * @param subBairro - nome do sub bairro
	 * @uml.property name="subBairro"
	 */
	public void setSubBairro(final String subBairro) {
		this.subBairro = subBairro;
	}

	/**
	 * @return  dataAlteração - data em que o bairro foi inserido na tabela para ser geocodificado
	 * @uml.property name="dataAtualizacao"
	 */
	public final Date getDataAlteracao() {
		if(dataAlteracao!=null){
			return new Date(dataAlteracao.getTime());
		}else{
			return null;
		}
	}

	/**
	 * @param dataAlteracao - data em que o bairro foi inserido na tabela para ser geocodificado
	 * @uml.property name="dataAtualizacao"
	 */
	public void setDataAlteracao(final Date dataAlteracao) {
		if(dataAlteracao!=null){
			this.dataAlteracao = new Date(dataAlteracao.getTime());
		}else{
			this.dataAlteracao = null;
		}
	}

}
