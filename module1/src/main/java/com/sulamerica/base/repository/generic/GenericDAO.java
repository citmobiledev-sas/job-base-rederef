package com.sulamerica.base.repository.generic;

import java.io.Serializable;
import java.util.List;

public interface GenericDAO<T, P extends Serializable> {

    /**
     * Save a entity T.
     *
     * @param ent
     */
    void save(T ent);

    /**
     * Get All entity T.
     *
     * @return
     */
    List<T> getAll();

    /**
     * Select entity by id.
     *
     * @param id
     * @return
     */
    T selectById(P id);

    /**
     * Delete entity.
     *
     * @param ent
     */
    void delete(T ent);

    /**
     * Deleta todos as instancias da entidade.
     */
    void deleteAll(List<T> ent);

    void saveAll(List<T> ent);

}
