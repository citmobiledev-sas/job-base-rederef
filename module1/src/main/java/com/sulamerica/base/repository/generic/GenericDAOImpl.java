package com.sulamerica.base.repository.generic;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManagerFactory;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

@Component
public abstract class GenericDAOImpl<T, P extends Serializable> implements GenericDAO<T, P> {


    @Autowired
    private EntityManagerFactory entityManagerFactory;

    private Class<T> entityClass;

    @SuppressWarnings({"unchecked", "rawtypes"})
    public GenericDAOImpl() {
        super();
        Type t = getClass().getGenericSuperclass();

        if (t instanceof ParameterizedType) {
            ParameterizedType pt = (ParameterizedType) t;
            entityClass = (Class) pt.getActualTypeArguments()[0];
        }
    }

    public void save(T ent) {
        entityManagerFactory.unwrap(SessionFactory.class).openSession().save(ent);
    }

    public void saveAll(java.util.List<T> ent) {
        for (T t : ent) {
            this.save(t);
        }
    }

    @SuppressWarnings("unchecked")
    public java.util.List<T> getAll() {
        return entityManagerFactory.unwrap(SessionFactory.class).openSession().createQuery(
                " from "
                        + entityClass.getName().substring(
                        entityClass.getName().lastIndexOf(".") + 1))
                .list();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * br.com.sulamerica.susis.fusiontable.sync.dao.GenericDAO#selectById(java
     * .io.Serializable)
     */
    @SuppressWarnings("unchecked")
    public T selectById(P id) {
        return (T) entityManagerFactory.unwrap(SessionFactory.class).openSession().get(entityClass.getName(), id);
    }

    public void delete(T ent) {
        entityManagerFactory.unwrap(SessionFactory.class).openSession().delete(ent);
    }

    public void deleteAll(java.util.List<T> ent) {
        for (T t : ent) {
            entityManagerFactory.unwrap(SessionFactory.class).openSession().delete(t);
        }
    }

    public void flush() {
        entityManagerFactory.unwrap(SessionFactory.class).openSession().flush();
    }

    public void update(T ent) {
        entityManagerFactory.unwrap(SessionFactory.class).openSession().update(ent);
    }

    public void merge(T ent) {
        entityManagerFactory.unwrap(SessionFactory.class).openSession().merge(ent);
    }

}
