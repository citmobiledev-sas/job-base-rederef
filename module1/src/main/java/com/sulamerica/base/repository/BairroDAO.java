package com.sulamerica.base.repository;

import com.sulamerica.base.model.RedeReferenciadaBairro;
import com.sulamerica.base.repository.generic.GenericDAOImpl;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManagerFactory;
import java.util.List;

@Component
public class BairroDAO extends GenericDAOImpl<RedeReferenciadaBairro, Long> {


    @Autowired
    private EntityManagerFactory entityManagerFactory;

    public List<RedeReferenciadaBairro> listarBairrosNaoGeocodificados() {

        StringBuffer sql = new StringBuffer();
        sql.append("SELECT B.* FROM SDE_BAIRRO_GEOREF B WHERE ROWNUM <= 10");

        // Monta a query de consulta de bairros a serem geolocalizados
        SQLQuery query = entityManagerFactory.unwrap(SessionFactory.class).openSession().createSQLQuery(sql.toString());
        query.addEntity(RedeReferenciadaBairro.class);

        // Executa a consulta
        List<RedeReferenciadaBairro> result = query.list();

        return result;

    }
}
