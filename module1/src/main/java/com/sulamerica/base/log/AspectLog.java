package com.sulamerica.base.log;

import com.sulamerica.base.Util.DateUtil;
import com.sulamerica.base.service.LogServices;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.net.UnknownHostException;

@Aspect
@Configuration
public class AspectLog {

    @Autowired
    private LogServices logServices;

    @Autowired
    private DateUtil dateUtil;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Pointcut para metodos da camada de serviço.
     */
    @Pointcut("execution(* com.sulamerica.base.service.*.*(..))" +
            " && !within(com.sulamerica.base.service.LogServices) " +
            " && !within(com.sulamerica.base.service.LogServicesImpl)")
    public void serviceMethods() {
    }

    /**
     * Pointcut para metodos da camada de dao
     */
    @Pointcut("execution(* com.sulamerica.base.repository.*.*(..))")
    public void daoMethods() {
    }

    /**
     * Metodo responsavel por executar antes de um metodo da camada de serviço.
     *
     * @param jp
     * @throws UnknownHostException
     */
    @Before("serviceMethods()")
    public void beforeMethod(JoinPoint jp) throws UnknownHostException {

        logger.info("Inicio servico " + jp.getSignature().getName() + " "
                + jp.getTarget().getClass().toString() + " " + dateUtil.getDateTimeNow() );

        logServices.logInicioGravacao(jp);

    }

    /**
     * Metodos responsavel por executar depois de um metodo da camada de
     * serviço.
     *
     * @param jp
     */
    @AfterReturning("serviceMethods()")
    public void afterMethod(JoinPoint jp) {

        logger.info("Fim servico " + jp.getSignature().getName() + " "
                + jp.getTarget().getClass().toString() + " " + dateUtil.getDateTimeNow());

//        logServices.logFimGravacao(jp);

    }

    /**
     * Metodo responsavel por logar exception da camada de serviço.
     *
     * @param jp
     * @param exception
     */
    @AfterThrowing(pointcut = "serviceMethods()", throwing = "exception")
    public void afterThrowing(JoinPoint jp, Throwable exception) {

        logger.error("Ocorreu uma excecao", exception);
//        logServices.logException(jp, exception);
    }

    /**
     * Metodo responsavel por logar exception na camada de dados.
     *
     * @param jp
     * @param exception
     */
    @AfterThrowing(pointcut = "daoMethods()", throwing = "exception")
    public void afterDAOThrowing(JoinPoint jp, Throwable exception) {

        logger.error("Ocorreu uma excecao no DAO.", exception);
//        logServices.logDAOException(jp, exception);
    }


}
