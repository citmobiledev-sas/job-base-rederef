package com.sulamerica.base;

import org.springframework.stereotype.Component;

@Component
public class MyService {

    public String message() {
        return "Hello World";
    }
}
